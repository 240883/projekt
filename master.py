from pymodbus.client import ModbusTcpClient
from pymodbus.payload import BinaryPayloadDecoder, Endian
from pymodbus.exceptions import ConnectionException
import time

client = ModbusTcpClient('localhost', 502)   # TCP připojení

while True:
    try:
        regs = client.read_input_registers(address = 0, count = 1)   # Cteni registru
    except ConnectionException:
        print("Cannot connect to server")
        time.sleep(3)
        continue
    if regs:
        decoder = BinaryPayloadDecoder.fromRegisters(regs.registers, byteorder=Endian.LITTLE)
        print(round(decoder.decode_16bit_float(), 2))
    else:
        print('Nelze cist registry')
    time.sleep(3)
