from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.server import StartTcpServer
from pymodbus.payload import BinaryPayloadBuilder, Endian
import threading
import time
import random


"""
Convert float number to registers-compatible value
"""
def encode_float(value: float):
    builder = BinaryPayloadBuilder(byteorder=Endian.LITTLE)
    builder.add_16bit_float(value)
    builder.build()
    return builder.to_registers()

def updater(dataBlock: ModbusSequentialDataBlock):
    while True:
        rand_value = round(random.uniform(0.0, 5.0), 2)
        print("writing: " + str(rand_value))
        dataBlock.setValues(1, encode_float(rand_value)) # include values from A/D instead of random value
        time.sleep(3)

if __name__ == "__main__":
    # Init modbus server context
    block = ModbusSequentialDataBlock(1, encode_float(0))
    context = ModbusSlaveContext(ir=block) # init only Input register (read-only), enough for this purpose
    server_context = ModbusServerContext(slaves=context, single=True)

    # Start background thread for updating the register value from sensor
    updating_thread = threading.Thread(target=updater, args=(block,))
    updating_thread.daemon=True
    updating_thread.start()

    StartTcpServer(context=server_context, address=("localhost", 502))